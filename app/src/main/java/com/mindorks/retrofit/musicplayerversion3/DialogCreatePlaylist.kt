package com.mindorks.retrofit.musicplayerversion3

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.widget.EditText
import android.widget.TextView

class DialogCreatePlaylist(context: Context, private val namePlaylist: String) : Dialog(context) {
    private val tvCancel: TextView by lazy {
        findViewById(R.id.tvCancel)
    }
    private lateinit var tvAccept: TextView
    private lateinit var etNamePlaylist: EditText
    private var callBack: onCallBackListenerDialog? = null

    init {
        setCancelable(false)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.create_play_list)
        tvAccept = findViewById(R.id.tvAccept)
        etNamePlaylist = findViewById(R.id.etNamePlaylist)
        etNamePlaylist.setText(namePlaylist)

        tvAccept.setOnClickListener {
            callBack?.onClickAccept(etNamePlaylist.text.toString())
        }

        tvCancel.setOnClickListener {
            callBack?.onClickCancel()
        }

    }

    fun setOnCallBackListenerDialong(callBackListenerDialog: onCallBackListenerDialog) {
        callBack = callBackListenerDialog
    }

    interface onCallBackListenerDialog {
        fun onClickAccept(namePlaylist: String)
        fun onClickCancel()
    }
}