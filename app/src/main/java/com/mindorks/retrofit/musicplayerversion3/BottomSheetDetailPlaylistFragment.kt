package com.mindorks.retrofit.musicplayerversion3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.FragmentDetailBottomSheetDialogBinding

class BottomSheetDetailPlaylistFragment (val song : SongStorage, val callBack : CallBackSelectBottomSheet? = null) : BottomSheetDialogFragment() {
    private lateinit var binding : FragmentDetailBottomSheetDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_detail_bottom_sheet_dialog,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.namePlaylistTv.text = song.nameSong

        binding.deletePlaylistTv.setOnClickListener {
            callBack?.onSelectRemove(song)
            dismiss()
        }
    }

    interface CallBackSelectBottomSheet {
        fun onSelectRemove(pathSong : SongStorage)
    }
}