package com.mindorks.retrofit.musicplayerversion3

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

sealed class MediaState()

object MediaLoading : MediaState()

data class MediaResult(val songs: List<SongStorage>) : MediaState()

class MediaStorage {

    private var _data = MutableStateFlow<List<SongStorage>>(emptyList())
    val data : StateFlow<List<SongStorage>> = _data
    private var _isLoading = MutableStateFlow(false)
    val isLoading : StateFlow<Boolean> = _isLoading

    private lateinit var context: Context
    private val scope = CoroutineScope(Dispatchers.IO)
    private var job: Job? = null


    fun init(context: Context) {
        this.context = context
        scanAudioFiles()
    }

    private fun onDataChanging() {
        scanAudioFiles()
    }

    private suspend fun startAudioFiles() = coroutineScope {
        _isLoading.value = true
        val list: ArrayList<SongStorage> = ArrayList()

        // Get the external storage media store audio uri
        val uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        //val uri: Uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI

        // IS_MUSIC : Non-zero if the audio file is music
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0"

        // Sort the musics
        val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"
        //val sortOrder = MediaStore.Audio.Media.TITLE + " DESC"

        // Query the external storage for music files
        val cursor = context.contentResolver.query(
            uri, // Uri
            null, // Projection
            selection, // Selection
            null, // Selection arguments
            sortOrder // Sort order
        )

        // If query result is not empty
        if (cursor != null && cursor.moveToFirst()) {
            val title: Int = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val path: Int = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            // Now loop through the music files
            try {
                do {
                    val audioTitle: String = cursor.getString(title)
                    val audioPath: String = cursor.getString(path)
                    // Add the current music to the list
                    list.add(SongStorage(audioTitle, audioPath))
                    _data.value = ArrayList(list)
                } while (cursor.moveToNext() && isActive)
                if (isActive) {
                    _isLoading.value = false
                }

            } finally {
                cursor.close()
            }
        }

    }

    private fun scanAudioFiles() {
        job?.cancel()
        job = scope.launch {
            startAudioFiles()
            job = null
        }
    }
}