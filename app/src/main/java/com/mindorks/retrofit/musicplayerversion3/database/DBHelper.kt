package com.mindorks.retrofit.musicplayerversion3.database

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mindorks.retrofit.musicplayerversion3.MyApplication
import com.mindorks.retrofit.musicplayerversion3.data.AppDatabase
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.database.entities.PlaylistEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.lang.reflect.Type

class DBHelper {

    companion object {
        val database by lazy { MyApplication.appContext.let { AppDatabase.getDatabase(it) } }
        private val musicPlayerDao = database.musicPlayerDao()

        suspend fun newPlaylist(name: String) {
            musicPlayerDao.insertPlayList(PlaylistEntity(name = name))
        }

        suspend fun updatePlaylistSong(id: Long, songs: List<SongStorage>) {
            musicPlayerDao.updatePlayListSongs(idPlaylist = id, encodeListSongs(songs))
        }

        suspend fun updatePlaylistName(id: Long, namePlaylist: String) {
            musicPlayerDao.updatePlayListName(idPlaylist = id, namePlaylist = namePlaylist)
        }

        fun getPlaylist(id: Long): Flow<Playlist?> {
            return musicPlayerDao.getAllDetailPlaylist(id).map {
                convertEntityToPlaylist(it)
            }
        }

        fun getAllListPlaylist(): Flow<List<Playlist>> {
            return musicPlayerDao.getAllListPlaylist().map {
                convertListEntityToPlaylists(it)
            }
        }

        suspend fun deletePlaylist(id: Long) {
            musicPlayerDao.deletePlayList(id)
        }

        fun queryPlaylistSongs(playlistId: Long): Flow<List<SongStorage>> {
            return musicPlayerDao.getAllDetailPlaylist(playlistId).map {
                if (it.storageSongs != ""){
                    decodeListSongs(it.storageSongs)
                } else {
                    emptyList()
                }

            }
        }

        private fun encodeListSongs(songs: List<SongStorage>): String {
            return Gson().toJson(songs)
        }

        private fun decodeListSongs(encodeString: String): List<SongStorage> {
            val collectionType: Type = object : TypeToken<List<SongStorage>>() {}.type
            return Gson().fromJson(encodeString, collectionType)
        }

        private fun convertListEntityToPlaylists(listEntity: List<PlaylistEntity>): List<Playlist> {
            val listPlaylist = ArrayList<Playlist>()
            listEntity.forEach {
                listPlaylist.add(Playlist(playlistId = it.id, playlistName = it.name))
            }

            return listPlaylist
        }

        private fun convertEntityToPlaylist(playlistEntity: PlaylistEntity): Playlist {
            return Playlist(playlistId = playlistEntity.id, playlistName = playlistEntity.name)
        }
    }

}