package com.mindorks.retrofit.musicplayerversion3.chooseSong

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mindorks.retrofit.musicplayerversion3.R
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.ChooseListMusicFragmentBinding

class ChooseListMusicFragment : Fragment(), CallbackItemChooseMusicOnClick {

    private val ARG_PLAYLIST_ID : String = "ARG_PLAYLIST_ID"
    private var playlistId : Long = 0
    companion object {
        fun newInstance(playlistId: Long) = ChooseListMusicFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_PLAYLIST_ID, playlistId)
            }
        }
    }

    private val viewModel: ChooseListMusicViewModel by viewModels{
        ChooseListMusicViewModelFatory(playlistId)
    }
    private lateinit var binding: ChooseListMusicFragmentBinding
    private lateinit var adapter: ChooseListAdapter
    private val addListSong : ArrayList<SongStorage> = ArrayList()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        playlistId = requireArguments().getLong(ARG_PLAYLIST_ID, -1)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.choose_list_music_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = ChooseListAdapter(this)
        binding.chooseMusicRv.adapter = adapter
        viewModel.listSongStorage().observe(viewLifecycleOwner,  Observer {
            adapter.submitList(it)
        })
        binding.addToPlaylistBt.setOnClickListener {
            viewModel.insertDetailPlaylist(addListSong)
            Toast.makeText(requireContext(), R.string.message_add_song_success, Toast.LENGTH_SHORT).show()
            requireActivity().onBackPressed()
        }
    }


    override fun onItemClickAddChooseMusic(song: SongStorage) {
        addListSong.add(song)
    }

    override fun onItemClickRemoveChooseMusic(song: SongStorage) {
        addListSong.remove(song)
    }
}