package com.mindorks.retrofit.musicplayerversion3

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ServiceMusic : Service() {
    // Binder given to clients
    private val binder = LocalBinder()
    private var CHANNEL_ID: String = ""
    private val ACTION_PAUSE = 1
    private val ACTION_RESUME = 2
    private val ACTION_PREVIOUS = 3
    private val ACTION_NEXT = 4
    private val scope = MainScope()
    companion object{
        private const val MUSIC_BUTTON_ACTION = "MUSIC_BUTTON_ACTION"
        private const val MUSIC_BUTTON_POSITION = "MUSIC_BUTTON_POSITION"
        private const val CHANNEL_NAME = "MediaPlayer Service"
        private const val MY_SERVICE = "my_service"
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): ServiceMusic = this@ServiceMusic
    }

    override fun onCreate() {
        scope.launch {
            PlayerController.isPlaying.collect {
                val nameSong = PlayerController.songCurrent.value?.nameSong
                val isPlaying = it
                if(nameSong != null){
                    sendNotification(nameSong, isPlaying)
                }
            }
        }

        scope.launch {
            PlayerController.songCurrent.collect {
                val nameSong = it?.nameSong
                val isPlaying = PlayerController.isPlaying.value
                if(nameSong != null){
                    sendNotification(nameSong, isPlaying)
                }
            }
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if(intent != null && intent.action == MUSIC_BUTTON_ACTION){
            val actionMusic = intent.getIntExtra(MUSIC_BUTTON_POSITION, 0)
            handleActionMusic(actionMusic)
        }
        return START_NOT_STICKY
    }

    private fun sendNotification(nameSong:String, isPlaying:Boolean) {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_music)
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        val flags = if(Build.VERSION.SDK_INT >= 23) PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT else PendingIntent.FLAG_UPDATE_CURRENT

        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            notificationIntent,
            flags
        )
        CHANNEL_ID = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(MY_SERVICE)
        } else {
            ""
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(nameSong)
            .setContentText(getString(R.string.content_text))
            .setSmallIcon(R.drawable.ic_music)
            .setLargeIcon(bitmap)
            .setContentIntent(pendingIntent)
            .setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setShowActionsInCompactView(0, 1, 2)
            )

             if (isPlaying) {
                notification.addAction(
                    R.drawable.previous_song, getString(R.string.text_previous), getPendingIntent(
                        this,
                        ACTION_PREVIOUS
                    )
                ).addAction(
                    R.drawable.ic_pause_song,
                    getString(R.string.text_pause),
                    getPendingIntent(this, ACTION_PAUSE)
                ).addAction(
                    R.drawable.next_song,
                    getString(R.string.text_next),
                    getPendingIntent(this, ACTION_NEXT)
                )
            } else {
                notification.addAction(
                    R.drawable.previous_song, getString(R.string.text_previous), getPendingIntent(
                        this,
                        ACTION_PREVIOUS
                    )
                ).addAction(
                    R.drawable.ic_play,
                    getString(R.string.text_pause),
                    getPendingIntent(this, ACTION_RESUME)
                ).addAction(
                    R.drawable.next_song,
                    getString(R.string.text_next),
                    getPendingIntent(this, ACTION_NEXT)
                )

            }

        startForeground(2, notification.build())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String): String {
        val chan = NotificationChannel(
            channelId,
            CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private fun getPendingIntent(context: Context, action: Int): PendingIntent {
        val intent = Intent(this, ServiceMusic::class.java)
        intent.action = MUSIC_BUTTON_ACTION
        intent.putExtra(MUSIC_BUTTON_POSITION, action)

        return PendingIntent.getService(
            context.applicationContext,
            action,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun handleActionMusic(action: Int?) {
        when (action) {
            ACTION_PAUSE,
            ACTION_RESUME -> {
                PlayerController.pauseOrResume()
            }
            ACTION_PREVIOUS -> PlayerController.previous()
            ACTION_NEXT -> PlayerController.next()
        }
    }
}