package com.mindorks.retrofit.musicplayerversion3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mindorks.retrofit.musicplayerversion3.databinding.FragmentPlaylistBottomSheetDialogBinding

class BottomSheetPlaylistFragment(val name : String, val callBack : CallBackSelectBottomSheet? = null) : BottomSheetDialogFragment() {
    private lateinit var binding : FragmentPlaylistBottomSheetDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_playlist_bottom_sheet_dialog,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.namePlaylistTv.text = name
        binding.addSongTv.setOnClickListener {
            callBack?.onSelectAdd()
            dismiss()
        }

        binding.editPlaylistTv.setOnClickListener {
            callBack?.onSelectEdit()
            dismiss()
        }

        binding.deletePlaylistTv.setOnClickListener {
            callBack?.onSelectRemove()
            dismiss()
        }
    }

    interface CallBackSelectBottomSheet {
        fun onSelectAdd()
        fun onSelectEdit()
        fun onSelectRemove()
    }
}