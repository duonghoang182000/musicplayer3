package com.mindorks.retrofit.musicplayerversion3.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mindorks.retrofit.musicplayerversion3.database.entities.PlaylistEntity

@Database(
    entities = [PlaylistEntity::class],
    version = 1,
    exportSchema = false
)
public abstract class AppDatabase : RoomDatabase() {
    abstract fun musicPlayerDao(): MusicPlayerDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDatabase? = null
        private const val NAME_DB = "music_player_database"

        fun getDatabase(
            context: Context
        ): AppDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    NAME_DB
                )
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}