package com.mindorks.retrofit.musicplayerversion3.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mindorks.retrofit.musicplayerversion3.R
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.PlaylistFragmentBinding
import com.mindorks.retrofit.musicplayerversion3.playmusic.BackgroundPlayMusicFragment

class ListSongFragment : Fragment(), SongAdapter.CallbackItemOnClick,
    SwipeRefreshLayout.OnRefreshListener {

    companion object {
        fun newInstance() = ListSongFragment()
    }

    private val viewModel: ListSongViewModel by viewModels()
    private lateinit var binding: PlaylistFragmentBinding
    private lateinit var adapter: SongAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.playlist_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        adapter = SongAdapter(this)
        binding.listSong.adapter = adapter
        binding.swipeRefreshLayout.setOnRefreshListener(this)
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.progress.visibility = View.VISIBLE
                binding.listSong.visibility = View.GONE
                viewModel.getListSongStorage()
            } else {
                binding.progress.visibility = View.GONE
                binding.listSong.visibility = View.VISIBLE
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
        viewModel.listSongStorage.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }

    override fun onItemClick(listSong: List<SongStorage>, position: Int) {

        requireActivity().supportFragmentManager.beginTransaction()
            .replace(
                R.id.flMain,
                BackgroundPlayMusicFragment.newInstance(listSong[position].path)
            )
            .addToBackStack("")
            .commit()
    }

    override fun onItemClickEdit(position: Int) {
        TODO("Not yet implemented")
    }

    override fun onRefresh() {
        viewModel.setRefresh()
    }

}