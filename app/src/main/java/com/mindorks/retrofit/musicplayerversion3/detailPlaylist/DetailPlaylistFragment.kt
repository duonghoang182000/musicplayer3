package com.mindorks.retrofit.musicplayerversion3.detailPlaylist

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.mindorks.retrofit.musicplayerversion3.BottomSheetDetailPlaylistFragment
import com.mindorks.retrofit.musicplayerversion3.R
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.DetailPlaylistFragmentBinding
import com.mindorks.retrofit.musicplayerversion3.playmusic.BackgroundPlaylistMusicFragment

class DetailPlaylistFragment : Fragment(), DetailPlaylistAdapter.CallbackItemChooseMusicOnClick,
    BottomSheetDetailPlaylistFragment.CallBackSelectBottomSheet {
    private val ARG_PLAYLIST_ID: String = "ARG_PLAYLIST_ID"
    private var playlistId: Long = 0

    companion object {
        fun newInstance(playlistId: Long) = DetailPlaylistFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_PLAYLIST_ID, playlistId)
            }
        }
    }

    private val viewModel: DetailPlaylistViewModel by viewModels {
        DetailPlaylistViewModelFatory(playlistId)
    }
    private lateinit var binding: DetailPlaylistFragmentBinding
    private lateinit var adapter: DetailPlaylistAdapter
    private lateinit var bottomSheetDetailPlaylistFragment: BottomSheetDetailPlaylistFragment

    override fun onAttach(context: Context) {
        super.onAttach(context)
        playlistId = requireArguments().getLong(ARG_PLAYLIST_ID)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.detail_playlist_fragment, container, false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        adapter = DetailPlaylistAdapter(this)
        binding.rvListMusic.adapter = adapter
        viewModel.getListSongDetailPlaylist()
        observerData()
        bindEvents()
    }

    private fun bindEvents() {
    }

    private fun observerData() {
        viewModel.listSong.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }

    override fun onItemChooseMusicClick(listSong: List<SongStorage>, position: Int) {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(
                R.id.flMain,
                BackgroundPlaylistMusicFragment.newInstance(playlistId, listSong[position].path)
            )
            .addToBackStack("")
            .commit()
    }

    override fun onItemClickEditChooseMusic(song: SongStorage, position: Int) {
        bottomSheetDetailPlaylistFragment = BottomSheetDetailPlaylistFragment(song, this)
        bottomSheetDetailPlaylistFragment.show(
            requireActivity().supportFragmentManager,
            bottomSheetDetailPlaylistFragment.tag
        )
    }

    override fun onSelectRemove(pathSong: SongStorage) {
        viewModel.deleteSongPlaylist(pathSong)
    }
}