package com.mindorks.retrofit.musicplayerversion3.data

data class SongStorage(val nameSong: String, val path: String)