package com.mindorks.retrofit.musicplayerversion3.chooseSong

import androidx.lifecycle.*
import com.mindorks.retrofit.musicplayerversion3.MyApplication
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.database.DBHelper
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ChooseListMusicViewModel(val playlistId: Long) : ViewModel() {
    val mediaStorage = MyApplication.mediaStorage
    val listSongStorage: MutableLiveData<List<SongStorage>> = MutableLiveData()

    fun insertDetailPlaylist(listSong : List<SongStorage>) = viewModelScope.launch {
        DBHelper.updatePlaylistSong(playlistId, listSong)
    }

    fun listSongStorage(): LiveData<List<SongStorage>> {
        viewModelScope.launch {
            mediaStorage.data.collect {
                listSongStorage.value = it
            }
        }
        return listSongStorage
    }
}

class ChooseListMusicViewModelFatory(val playlistId: Long) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(ChooseListMusicViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return ChooseListMusicViewModel(playlistId) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }

}