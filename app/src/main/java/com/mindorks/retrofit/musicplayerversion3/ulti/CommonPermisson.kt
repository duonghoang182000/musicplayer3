package com.mindorks.retrofit.musicplayer.ulti

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager

import android.os.Build
import android.view.View
import com.google.android.material.snackbar.Snackbar


fun Context.checkPermissionForReadExtertalStorage(): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val result = this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }
    return false
}


