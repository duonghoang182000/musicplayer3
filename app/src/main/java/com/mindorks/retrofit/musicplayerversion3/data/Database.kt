package com.mindorks.retrofit.musicplayerversion3.data


data class Playlist(
    val playlistId: Long = 0L,
    var playlistName: String = "",
)