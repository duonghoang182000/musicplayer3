package com.mindorks.retrofit.musicplayerversion3.data

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.mindorks.retrofit.musicplayerversion3.database.entities.PlaylistEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MusicPlayerDao {
    @Insert
    suspend fun insertPlayList(playlist: PlaylistEntity)

    @Query("UPDATE PlaylistEntity SET name_playlist = :namePlaylist WHERE playlist_id = :idPlaylist")
    suspend fun updatePlayListName(idPlaylist: Long, namePlaylist : String)

    @Query("UPDATE PlaylistEntity SET listPathSong = :songs WHERE playlist_id = :idPlaylist")
    suspend fun updatePlayListSongs(idPlaylist: Long, songs : String)

    @Query("DELETE FROM  PlaylistEntity WHERE playlist_id = :idPlaylist")
    suspend fun deletePlayList(idPlaylist: Long)

    @Query("SELECT * FROM  PlaylistEntity")
    fun getAllListPlaylist(): Flow<List<PlaylistEntity>>

    @Query("SELECT * FROM  PlaylistEntity WHERE playlist_id = :idPlaylist")
    fun getAllDetailPlaylist(idPlaylist: Long): Flow<PlaylistEntity>



}