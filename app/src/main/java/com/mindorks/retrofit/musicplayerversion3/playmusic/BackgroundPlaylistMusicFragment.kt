package com.mindorks.retrofit.musicplayerversion3.playmusic

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.mindorks.retrofit.musicplayerversion3.R
import com.mindorks.retrofit.musicplayerversion3.databinding.BackgroudPlayMusicFragmentBinding
import com.mindorks.retrofit.musicplayerversion3.databinding.BackgroudPlaylistMusicFragmentBinding

class BackgroundPlaylistMusicFragment : Fragment() {

    companion object {
        private val ARG_PATH_SONG: String = "ARG_PATH_SONG"
        private val ARG_PLAYLIST_ID: String = "ARG_PLAYLIST_ID"

        fun newInstance(playListId: Long, pathSong: String) =
            BackgroundPlaylistMusicFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PATH_SONG, pathSong)
                    putLong(ARG_PLAYLIST_ID, playListId)
                }
            }
    }

    private lateinit var binding: BackgroudPlaylistMusicFragmentBinding

    private val viewModel: BackgroundPlaylistMusicViewModel by viewModels {
        BackgroundPlaylistMusicViewModelFactory(
            playlistId = requireArguments().getLong(ARG_PLAYLIST_ID),
            pathSong = requireArguments().getString(ARG_PATH_SONG)!!,
            application = requireActivity().application
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.backgroud_playlist_music_fragment,
            container,
            false
        )
        return binding.root
    }

    private fun observerData() {
        viewModel.isPlaying.observe(viewLifecycleOwner) {
            if (it) {
                binding.playButton.setImageResource(R.drawable.ic_pause_song)
            } else {
                binding.playButton.setImageResource(R.drawable.ic_play)
            }
        }

        viewModel.isLooping.observe(viewLifecycleOwner) {
            if (it) {
                binding.loopButton.setImageResource(R.drawable.ic_repeat_one)
            } else {
                binding.loopButton.setImageResource(R.drawable.ic_repeat_all_off)
            }
        }

        viewModel.isNextAble.observe(viewLifecycleOwner) {
            if (it) {
                binding.nextButton.visibility = View.VISIBLE
            } else {
                binding.nextButton.visibility = View.GONE
            }
        }

        viewModel.isPreviousAble.observe(viewLifecycleOwner) {
            if (it) {
                binding.previousButton.visibility = View.VISIBLE
            } else {
                binding.previousButton.visibility = View.GONE
            }
        }

        viewModel.isShuffling.observe(viewLifecycleOwner) {
            if (it) {
                binding.shuffleButton.setImageResource(R.drawable.ic_shuffle_on)
            } else {
                binding.shuffleButton.setImageResource(R.drawable.ic_shuffle_off)
            }
        }

        viewModel.duration.observe(viewLifecycleOwner) {
            binding.duration.text = viewModel.createTimerLabel(it)
            binding.durationSb.max = it
        }

        viewModel.timeCurrent.observe(viewLifecycleOwner) {
            binding.timeCurrent.text = viewModel.createTimerLabel(it)
            binding.durationSb.progress = it
        }

        viewModel.songCurrent.observe(viewLifecycleOwner) {
            binding.song = it
        }


    }

    private fun bindEvents() {
        binding.nextButton.setOnClickListener {
            viewModel.clickedOnNextButton()
        }

        binding.previousButton.setOnClickListener {
            viewModel.clickedOnPreviousButton()
        }

        binding.playButton.setOnClickListener {
            viewModel.clickedOnPlayButton()
        }

        binding.shuffleButton.setOnClickListener {
            viewModel.clickedOnShuffling()
        }

        binding.loopButton.setOnClickListener {
            viewModel.clickedOnLooping()
        }

        binding.durationSb.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar?,
                progressValue: Int,
                fromUser: Boolean
            ) {
//                viewModel.setToTimeCurrent(progressValue)
                binding.durationSb.progress = progressValue
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                seekBar?.progress?.let { viewModel.setToTimeCurrent(it) }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerData()
        bindEvents()
    }

}