package com.mindorks.retrofit.musicplayerversion3.playlist

import android.util.Log
import androidx.lifecycle.*
import com.mindorks.retrofit.musicplayerversion3.*
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ListSongViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private val mediaStorage = MyApplication.mediaStorage
    private var _listSongStorage: MutableLiveData<List<SongStorage>> = MutableLiveData()
    val listSongStorage: LiveData<List<SongStorage>> get() = _listSongStorage
    val isLoading: LiveData<Boolean> get() = mediaStorage.isLoading.asLiveData(viewModelScope.coroutineContext)

    init {
        getListSongStorage()
    }

    fun getListSongStorage() {
        viewModelScope.launch {
            mediaStorage.data.collect {
                    _listSongStorage.value = it
            }
        }
    }

    fun setRefresh() {
        mediaStorage.init(MyApplication.appContext)
    }
}