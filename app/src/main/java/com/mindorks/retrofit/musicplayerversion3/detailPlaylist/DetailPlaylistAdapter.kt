package com.mindorks.retrofit.musicplayerversion3.detailPlaylist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.ItemDetailPlaylistBinding

class DetailPlaylistAdapter(val callbackItemOnClick: CallbackItemChooseMusicOnClick? = null) :
    ListAdapter<SongStorage, DetailPlaylistAdapter.DetailPlaylistViewHolder>(
        DetailPlaylistAdapterCallBack()
    ) {

    inner class DetailPlaylistViewHolder(private val bindingDetailPlaylistBinding: ItemDetailPlaylistBinding) :
        RecyclerView.ViewHolder(bindingDetailPlaylistBinding.root) {
        fun onBind(item: SongStorage) {
            bindingDetailPlaylistBinding.song = item
            bindingDetailPlaylistBinding.ivEdit.setOnClickListener {
                callbackItemOnClick?.onItemClickEditChooseMusic(item, bindingAdapterPosition)
            }
            bindingDetailPlaylistBinding.clItemSong.setOnClickListener {
                callbackItemOnClick?.onItemChooseMusicClick(currentList, bindingAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailPlaylistViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemDetailPlaylistBinding.inflate(layoutInflater, parent, false)
        return DetailPlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DetailPlaylistViewHolder, position: Int) {
        val item = getItem(holder.bindingAdapterPosition)
        holder.onBind(item)
    }

    interface CallbackItemChooseMusicOnClick {
        fun onItemChooseMusicClick(listSong: List<SongStorage>, position: Int)
        fun onItemClickEditChooseMusic(song: SongStorage, position: Int)
    }
}


class DetailPlaylistAdapterCallBack : DiffUtil.ItemCallback<SongStorage>() {
    override fun areItemsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem == newItem
    }

}