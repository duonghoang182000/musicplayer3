package com.mindorks.retrofit.musicplayerversion3.chooseSong

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.ItemChooseMusicBinding

class ChooseListAdapter(val callBack: CallbackItemChooseMusicOnClick? = null) :
    ListAdapter<SongStorage, ChooseListAdapter.ChooseListViewHolder>(ChooseListAdapterCallBack()) {

    inner class ChooseListViewHolder(val bindingChooseMusicBinding: ItemChooseMusicBinding) :
        RecyclerView.ViewHolder(bindingChooseMusicBinding.root) {
        fun onBind(item: SongStorage) {
            bindingChooseMusicBinding.song = item
            bindingChooseMusicBinding.ivAdd.setOnClickListener { it ->
                it.isSelected = !it.isSelected
                if (bindingChooseMusicBinding.ivAdd.isSelected) {
                    callBack?.onItemClickAddChooseMusic(item)
                } else {
                    callBack?.onItemClickRemoveChooseMusic(item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemChooseMusicBinding.inflate(layoutInflater, parent, false)
        return ChooseListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ChooseListViewHolder, position: Int) {
        val item = getItem(holder.bindingAdapterPosition)
        holder.onBind(item)
    }
}

interface CallbackItemChooseMusicOnClick {
    fun onItemClickAddChooseMusic(song: SongStorage)
    fun onItemClickRemoveChooseMusic(song: SongStorage)
}


class ChooseListAdapterCallBack : DiffUtil.ItemCallback<SongStorage>() {
    override fun areItemsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem.path == newItem.path
    }

    override fun areContentsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem == newItem
    }

}