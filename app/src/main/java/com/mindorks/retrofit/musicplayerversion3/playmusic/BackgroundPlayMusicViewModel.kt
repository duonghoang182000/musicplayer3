package com.mindorks.retrofit.musicplayerversion3.playmusic

import android.app.Application
import androidx.lifecycle.*
import com.mindorks.retrofit.musicplayerversion3.MyApplication
import com.mindorks.retrofit.musicplayerversion3.PlayerController
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class BackgroundPlayMusicViewModel(pathSong: String, application: Application) :
    AndroidViewModel(application) {
    val mediaStorage = MyApplication.mediaStorage
    val isPlaying: LiveData<Boolean> = PlayerController.isPlaying.asLiveData(getCoroutineContext())
    val isLooping: LiveData<Boolean> = PlayerController.isLooping.asLiveData(getCoroutineContext())
    val isPreviousAble: LiveData<Boolean> =
        PlayerController.isPreviousAble.asLiveData(getCoroutineContext())
    val isNextAble: LiveData<Boolean> =
        PlayerController.isNextAble.asLiveData(getCoroutineContext())
    val timeCurrent: LiveData<Int> = PlayerController.timeCurrent.asLiveData(getCoroutineContext())
    val duration: LiveData<Int> = PlayerController.duration.asLiveData(getCoroutineContext())
    val songCurrent: LiveData<SongStorage?> = PlayerController.songCurrent.asLiveData(getCoroutineContext())
    val onError: LiveData<Boolean> = PlayerController.onError.asLiveData(getCoroutineContext())
    val isShuffling: LiveData<Boolean> =
        PlayerController.isShuffle.asLiveData(getCoroutineContext())
    private var firstPlay : Boolean = true

    init {
        viewModelScope.launch {
            mediaStorage.data.collect {
                PlayerController.setListSong(it)
                if(firstPlay){
                    PlayerController.play(pathSong)
                    firstPlay = false
                }
            }
        }
    }

    private fun getCoroutineContext(): CoroutineContext = viewModelScope.coroutineContext

    fun setListSong(listSong: List<SongStorage>) {
        PlayerController.setListSong(listSong)
    }

    fun clickedOnNextButton() {
        PlayerController.next()
    }

    fun clickedOnPreviousButton() {
        PlayerController.previous()
    }

    fun clickedOnShuffling() {
        PlayerController.setShuffling(!PlayerController.isShuffle.value)
    }

    fun clickedOnLooping() {
        PlayerController.setLooping(!PlayerController.isLooping.value)
    }

    fun clickedOnPlayButton() {
        PlayerController.pauseOrResume()
    }

    fun createTimerLabel(duration: Int): String? {
        var time: String? = ""
        val min = duration / 1000 / 60
        val sec = duration / 1000 % 60
        time += "$min:"
        if (sec < 10) {
            time += "0"
        }
        time += sec
        return time
    }

    fun setToTimeCurrent(progressValue: Int) {
        PlayerController.setToTimeCurrent(progressValue)
    }
}

class BackgroundPlayMusicViewModelFactory(
    private val pathSong: String,
    private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BackgroundPlayMusicViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BackgroundPlayMusicViewModel(pathSong, application) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}
