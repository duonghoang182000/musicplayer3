package com.mindorks.retrofit.musicplayerversion3.playlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.databinding.ItemPlaylistBinding

class PlayListAdapter(val callbackItemOnClick: CallbackItemPlaylistOnClick? = null) :
    ListAdapter<Playlist, PlayListAdapter.PlaylistViewHolder>(PlaylistAdapterCallBack()) {

    inner class PlaylistViewHolder(val bindingPlaylistBinding: ItemPlaylistBinding) :
        RecyclerView.ViewHolder(bindingPlaylistBinding.root) {
        fun onBind(item: Playlist) {
            bindingPlaylistBinding.playlist = item
            bindingPlaylistBinding.ivEdit.setOnClickListener {
                callbackItemOnClick?.onItemClickEditPlaylist(item)
            }
            bindingPlaylistBinding.clPlaylist.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    callbackItemOnClick?.onItemPlaylistClick(item)
                }
            })
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemPlaylistBinding.inflate(layoutInflater, parent, false)
        return PlaylistViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val item = getItem(holder.bindingAdapterPosition)
        holder.onBind(item)
    }

    override fun onBindViewHolder(
        holder: PlaylistViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }

    interface CallbackItemPlaylistOnClick {
        fun onItemPlaylistClick(playlist: Playlist)
        fun onItemClickEditPlaylist(playlist: Playlist)
    }
}


class PlaylistAdapterCallBack : DiffUtil.ItemCallback<Playlist>() {
    override fun areItemsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem.playlistName == newItem.playlistName
    }

    override fun areContentsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }

}