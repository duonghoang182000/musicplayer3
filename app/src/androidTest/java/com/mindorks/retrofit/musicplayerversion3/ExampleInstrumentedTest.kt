package com.mindorks.retrofit.musicplayerversion3

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    val lvData = MutableLiveData<Int>(1)
    val flowData = flow<Int> {
        for (i in 0..10){
            delay(1000)
            emit(i)
        }


    }


    @Test
    fun useAppContext(){
        runBlocking(Dispatchers.Main){
            launch{
                /* lvData.observeForever{
                     Log.d("taih", "job1 ${it} ")
                    launch {
                        delay(1000)
                        lvData.value = it + 1
                    }

                 }*/
                delay(2000)
                flowData.collect {
                    Log.d("taih", "job1 ${it}" )
                }
            }

            launch{
                flowData.collect {
                    Log.d("taih", "job2 ${it}" )
                }
            }
        }



    }
}