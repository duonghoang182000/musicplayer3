package com.mindorks.retrofit.musicplayerversion3

import android.app.Application
import android.content.Context
import android.util.Log
import com.mindorks.retrofit.musicplayer.ulti.checkPermissionForReadExtertalStorage
import com.mindorks.retrofit.musicplayerversion3.data.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process

    companion object {
        private var _appContext: Context? = null
        val appContext: Context get() = _appContext!!
        // Using by lazy so the database and the repository are only created when they're needed
        // rather than when the application starts
        val mediaStorage by lazy { MediaStorage() }
    }



    override fun onCreate() {
        super.onCreate()
        _appContext = this
        if(_appContext!!.checkPermissionForReadExtertalStorage()){
            mediaStorage.init(_appContext as MyApplication)
        }
    }
}