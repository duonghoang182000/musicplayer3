package com.mindorks.retrofit.musicplayerversion3.home

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mindorks.retrofit.musicplayerversion3.BottomSheetPlaylistFragment
import com.mindorks.retrofit.musicplayerversion3.DialogCreatePlaylist
import com.mindorks.retrofit.musicplayerversion3.R
import com.mindorks.retrofit.musicplayerversion3.chooseSong.ChooseListMusicFragment
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.databinding.HomeFragmentBinding
import com.mindorks.retrofit.musicplayerversion3.detailPlaylist.DetailPlaylistFragment
import com.mindorks.retrofit.musicplayerversion3.playlist.ListSongFragment
import com.mindorks.retrofit.musicplayerversion3.playlist.PlayListAdapter

class HomeFragment : Fragment() , PlayListAdapter.CallbackItemPlaylistOnClick
    , DialogCreatePlaylist.onCallBackListenerDialog
    , BottomSheetPlaylistFragment.CallBackSelectBottomSheet
{

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var binding : HomeFragmentBinding
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var adapter: PlayListAdapter
    private lateinit var dialogCreatePlaylist: DialogCreatePlaylist
    private lateinit var dialogEditPlaylist: DialogCreatePlaylist
    private lateinit var bottomSheetFragment : BottomSheetPlaylistFragment


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.home_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = PlayListAdapter(this)
        binding.listPlaylistRv.adapter = adapter
        observerData()
        bindEvents()
    }

    private fun bindEvents() {
        binding.createPlaylistView.setOnClickListener {
            showDialogCreatePlaylist()
        }

        binding.musicList.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.flMain, ListSongFragment.newInstance())
                .addToBackStack("")
                .commit()
             //   Navigation.createNavigateOnClickListener(R.id.action_list_song_to_backgroudPlayMusicFragment, null)
        }
    }

    private fun showDialogCreatePlaylist() {
        dialogCreatePlaylist = DialogCreatePlaylist(requireContext(), "")
        dialogCreatePlaylist.setOnCallBackListenerDialong(this)
        dialogCreatePlaylist.show()
    }

    private fun showDialogEditPlaylist(namePlaylist: String) {
        dialogEditPlaylist = DialogCreatePlaylist(requireContext(), namePlaylist)
        dialogEditPlaylist.setOnCallBackListenerDialong(object : DialogCreatePlaylist.onCallBackListenerDialog{
            override fun onClickAccept(namePlaylist: String) {
                binding.playlist?.let {
                    viewModel.updatePlaylistName(it.playlistId, namePlaylist)
                }
                dialogEditPlaylist.dismiss()
            }

            override fun onClickCancel() {
                dialogEditPlaylist.dismiss()
            }

        })
        dialogEditPlaylist.show()
    }

    private fun observerData() {
        viewModel.getListPlaylist().observe(viewLifecycleOwner){
            adapter.submitList(it)
        }
    }

    override fun onItemPlaylistClick(playlist: Playlist) {
        binding.playlist = playlist
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.flMain, DetailPlaylistFragment.newInstance(binding.playlist!!.playlistId))
            .addToBackStack("")
            .commit()
    }

    override fun onItemClickEditPlaylist(playlist: Playlist) {
        binding.playlist = playlist
        bottomSheetFragment = BottomSheetPlaylistFragment(playlist.playlistName, this)
        bottomSheetFragment.show(requireActivity().supportFragmentManager, bottomSheetFragment.tag)
    }

    override fun onClickAccept(namePlaylist : String) {
        viewModel.insertPlaylist(namePlaylist)
        dialogCreatePlaylist.let { dialogCreatePlaylist.dismiss() }
    }

    override fun onClickCancel() {
        dialogCreatePlaylist.dismiss()
    }

    fun showDialogDeletePlaylist() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.delete_play_list)
        val tvNamePlaylist = dialog.findViewById<TextView>(R.id.tvNamePlaylist)
        tvNamePlaylist.text = resources.getString(R.string.text_question_delete_playlist, binding.playlist?.playlistName )
        dialog.findViewById<TextView>(R.id.btCancel).setOnClickListener { dialog.dismiss() }
        dialog.findViewById<TextView>(R.id.btAccept).setOnClickListener {
            binding.playlist?.let { viewModel.deletePlaylist(it.playlistId) }
            dialog.dismiss()

        }

        dialog.show()
    }

    override fun onSelectAdd() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.flMain, ChooseListMusicFragment.newInstance(binding.playlist!!.playlistId))
            .addToBackStack("")
            .commit()
    }

    override fun onSelectEdit() {
        binding.playlist?.let { showDialogEditPlaylist(it.playlistName) }
    }

    override fun onSelectRemove() {
        showDialogDeletePlaylist()
    }
}