package com.mindorks.retrofit.musicplayerversion3

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.core.app.ActivityCompat
import com.google.android.material.snackbar.Snackbar
import com.mindorks.retrofit.musicplayer.ulti.checkPermissionForReadExtertalStorage
import com.mindorks.retrofit.musicplayerversion3.home.HomeFragment
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class MainActivity : AppCompatActivity() {
    private val REQUEST_PERMISSION_SETTING: Int = 1802

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!this.checkPermissionForReadExtertalStorage()) {
            requestPermissionForReadExtertalStorage()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.flMain, HomeFragment.newInstance())
                .commitAllowingStateLoss()
        }
        val intent = Intent(this, ServiceMusic::class.java)
        startService(intent)
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (i in permissions.indices) {
            val permission: String = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                // user rejected the permission
                val showRationale: Boolean =
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        this.shouldShowRequestPermissionRationale(permission)
                    } else {
                        TODO("VERSION.SDK_INT < M")
                    };
                if (!showRationale) {
                    // user also CHECKED "never ask again"
                    // you can either enable some fall back,
                    // disable features of your app
                    // or open another dialog explaining
                    // again the permission and directing to
                    // the app setting
                    val snackbar: Snackbar = Snackbar
                        .make(findViewById(R.id.clMain), "Không có mạng", Snackbar.LENGTH_LONG)
                        .setAction("Thử lại", object : View.OnClickListener {
                            override fun onClick(p0: View?) {
                                val intent = Intent()
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri: Uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivityForResult(intent, REQUEST_PERMISSION_SETTING)
                            }

                        })
                    snackbar.show()
                } else if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission)) {
                    // user did NOT check "never ask again"
                    // this is a good place to explain the user
                    // why you need the permission and ask if he wants
                    // to accept it (the rationale)
                    requestPermissionForReadExtertalStorage()
                }
            } else {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.flMain, HomeFragment.newInstance())
                    .commitAllowingStateLoss()
            }
        }
    }

    @Throws(Exception::class)
    fun requestPermissionForReadExtertalStorage() {
        try {
            ActivityCompat.requestPermissions(
                (this as Activity?)!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                1239
            )
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
    
}

