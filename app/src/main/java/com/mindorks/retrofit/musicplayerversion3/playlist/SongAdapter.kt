package com.mindorks.retrofit.musicplayerversion3.playlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.databinding.ItemSongBinding

class SongAdapter(val callbackItemOnClick: CallbackItemOnClick? = null) :
    ListAdapter<SongStorage, SongAdapter.SongViewHolder>(SongAdapterCallBack()) {

    inner class SongViewHolder(val bindingSongBinding: ItemSongBinding) :
        RecyclerView.ViewHolder(bindingSongBinding.root) {
        fun onBind(item: SongStorage) {
            bindingSongBinding.song = item
            bindingSongBinding.clItemSong.setOnClickListener {
                callbackItemOnClick?.onItemClick(currentList, bindingAdapterPosition)
            }
            bindingSongBinding.ivEdit.setOnClickListener {
                callbackItemOnClick?.onItemClickEdit(bindingAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSongBinding.inflate(layoutInflater, parent, false)
        return SongViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val item = getItem(holder.bindingAdapterPosition)
        holder.onBind(item)

    }

    interface CallbackItemOnClick {
        fun onItemClick(listSong: List<SongStorage>, position: Int)
        fun onItemClickEdit(position: Int)
    }
}

class SongAdapterCallBack : DiffUtil.ItemCallback<SongStorage>() {
    override fun areItemsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: SongStorage, newItem: SongStorage): Boolean {
        return oldItem == newItem
    }

}