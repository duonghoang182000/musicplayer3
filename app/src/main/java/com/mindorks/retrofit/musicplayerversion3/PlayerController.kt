package com.mindorks.retrofit.musicplayerversion3

import android.media.MediaPlayer
import android.net.Uri
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

object PlayerController {
    private var _isPlaying = MutableStateFlow(false)
    val isPlaying: StateFlow<Boolean> = _isPlaying
    private var _isShuffle = MutableStateFlow(false)
    val isShuffle: StateFlow<Boolean> = _isShuffle
    private var _isLooping = MutableStateFlow(false)
    val isLooping: StateFlow<Boolean> = _isLooping
    private var _isNextAble = MutableStateFlow(true)
    val isNextAble: StateFlow<Boolean> = _isNextAble
    private var _isPreviousAble = MutableStateFlow(true)
    val isPreviousAble: StateFlow<Boolean> = _isPreviousAble
    private var _timeCurrent = MutableStateFlow(0)
    val timeCurrent: StateFlow<Int> = _timeCurrent
    private var _duration = MutableStateFlow(0)
    val duration: StateFlow<Int> = _duration
    private var _songCurrent = MutableStateFlow<SongStorage?>(null)
    val songCurrent: StateFlow<SongStorage?> = _songCurrent
    private var _listSongStorage = MutableStateFlow<List<SongStorage>>(emptyList())
    val listSongStorage: StateFlow<List<SongStorage>> = _listSongStorage
    private var _onError = MutableStateFlow(false)
    val onError: StateFlow<Boolean> = _onError
    private var positionCurrentSong: Int = 0
    private var mediaPlayer: MediaPlayer? = null
    private val mainScope = MainScope()
    private var timeJob: Job? = null

    fun setListSong(list: List<SongStorage>) {
        _listSongStorage.value = list
    }

    fun addSong(song: SongStorage) {
        val data = ArrayList(listSongStorage.value)
        data.add(song)
        _listSongStorage.value = data
    }

    fun removeSong(song: SongStorage) {
        val data = ArrayList(listSongStorage.value)
        data.remove(song)
        _listSongStorage.value = data
    }

    fun pauseOrResume() {
        val mediaPlayer = this.mediaPlayer ?: return
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()

        } else {
            mediaPlayer.seekTo(mediaPlayer.currentPosition)
            mediaPlayer.start()
            startTimerIfNotReady()
        }
    }

    fun next() {
        if (isNextSong()) {
            positionCurrentSong++
        } else {
            positionCurrentSong = 0
        }
        play(listSongStorage.value[positionCurrentSong].path)
    }

    fun previous() {
        if (isPreviousSong()) {
            positionCurrentSong--
        } else {
            positionCurrentSong = listSongStorage.value.size.minus(1) ?: 0
        }
        play(listSongStorage.value[positionCurrentSong].path)
    }

    private fun isNextSong(): Boolean {
        return positionCurrentSong < listSongStorage.value.size.minus(1) ?: 0
    }

    private fun isPreviousSong(): Boolean {
        return positionCurrentSong != 0
    }


    fun setLooping(isLooping: Boolean) {
        mediaPlayer?.isLooping = isLooping
        _isLooping.value = isLooping
    }

    fun setShuffling(isShuffling: Boolean) {
        mediaPlayer?.isLooping = false
        _isLooping.value = false
        _isShuffle.value = isShuffling

    }

    fun play(pathSong: String) {
        mediaPlayer?.stop()
        var currentSong: SongStorage? = null
        listSongStorage.value.forEachIndexed { index, songStorage ->
            if (songStorage.path == pathSong) {
                positionCurrentSong = index
                currentSong = songStorage
            }
        }
        val newCurrentSong = currentSong ?: return
        val context = MyApplication.appContext
        _songCurrent.value = currentSong
        val uri: Uri = Uri.parse(newCurrentSong.path)
        mediaPlayer = MediaPlayer()
        mediaPlayer?.setDataSource(context, uri)
        mediaPlayer?.prepareAsync();
        mediaPlayer?.setOnPreparedListener {
            mediaPlayer?.start()
        }
        startTimerIfNotReady()


    }

    private fun startTimerIfNotReady() {
        if (timeJob == null) {
            timeJob = mainScope.launch {
                while (isActive) {
                    delay(250)
                    val mediaPlayer = mediaPlayer ?: return@launch

                    val isPlaying = mediaPlayer.isPlaying
                    if (this@PlayerController.isPlaying.value != isPlaying) {
                        _isPlaying.value = mediaPlayer.isPlaying
                    }
                    if (this@PlayerController.isLooping.value != mediaPlayer.isLooping) {
                        _isLooping.value = mediaPlayer.isLooping
                    }
                    if (timeCurrent.value != mediaPlayer.currentPosition) {
                        _timeCurrent.value = (mediaPlayer.currentPosition)
                    }

                    if (duration.value != mediaPlayer.duration) {
                        _duration.value = (mediaPlayer.duration)
                    }

                    if (!isPlaying) stopTimer()
                }
            }
        }
    }

    private fun stopTimer() {
        timeJob?.cancel()
        timeJob = null
    }

    fun clear() {
        _listSongStorage.value = (emptyList())
    }

    private fun getSong(pos: Int): SongStorage? {
        return listSongStorage.value.getOrElse(pos) {
            null
        }
    }

    private fun size(): Int {
        return listSongStorage.value.size
    }

    fun setToTimeCurrent(progressValue: Int) {
        mediaPlayer?.seekTo(progressValue)
    }

}