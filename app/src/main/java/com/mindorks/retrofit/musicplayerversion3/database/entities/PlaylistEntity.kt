package com.mindorks.retrofit.musicplayerversion3.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PlaylistEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "playlist_id")
    val id:Long = 0L,
    @ColumnInfo(name = "name_playlist")
    val name:String,
    @ColumnInfo(name = "listPathSong")
    val storageSongs:String = "")
