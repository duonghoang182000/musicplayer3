package com.mindorks.retrofit.musicplayerversion3.detailPlaylist

import androidx.lifecycle.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mindorks.retrofit.musicplayerversion3.MyApplication
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.data.SongStorage
import com.mindorks.retrofit.musicplayerversion3.database.DBHelper
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class DetailPlaylistViewModel(val playlistId: Long) : ViewModel() {
    val _listSong = MutableLiveData<List<SongStorage>>()
    val listSong : LiveData<List<SongStorage>> get() = _listSong

    fun getListSongDetailPlaylist() = viewModelScope.launch{
        DBHelper.queryPlaylistSongs(playlistId).collect {
            _listSong.value = it
        }
    }

    fun deleteSongPlaylist(song : SongStorage) = viewModelScope.launch {
        val songs : ArrayList<SongStorage> = ArrayList(_listSong.value)
        songs.remove(song)
        _listSong.value = songs
        DBHelper.updatePlaylistSong(playlistId, songs)
    }
}
class DetailPlaylistViewModelFatory(val playlistId: Long) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(DetailPlaylistViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return DetailPlaylistViewModel(playlistId) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }

}
