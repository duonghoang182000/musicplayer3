package com.mindorks.retrofit.musicplayerversion3.home

import androidx.lifecycle.*
import com.mindorks.retrofit.musicplayerversion3.data.Playlist
import com.mindorks.retrofit.musicplayerversion3.database.DBHelper
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    fun getListPlaylist() : LiveData<List<Playlist>> {
        return DBHelper.getAllListPlaylist().asLiveData(viewModelScope.coroutineContext)
    }

    fun insertPlaylist(namePlaylist: String) = viewModelScope.launch {
        DBHelper.newPlaylist(namePlaylist)
    }

    fun deletePlaylist(playlistId: Long) = viewModelScope.launch{
        DBHelper.deletePlaylist(playlistId)
    }

    fun updatePlaylistName(playlistId: Long, namePlaylist: String) = viewModelScope.launch {
        DBHelper.updatePlaylistName(playlistId, namePlaylist)
    }
}